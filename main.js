
$( function () {

    var $uiGalleries = $('.ui-galleries');
    var $uiGalleriesBtn = $('.ui-galleries-btn');
    var template = $('#galleryTemplate').html();

    $uiGalleries.masonry({
        itemSelector: '.ui-galleries-item',
        columnWidth: '.ui-galleries-sizer',
        percentPosition: true
    });

    reAdjustLayout();

    $(window).on('resize', reAdjustLayout);

    $uiGalleriesBtn.on('click', function (e) {
        e.preventDefault();

        var url = $(this).attr('data-next');

        $.getJSON(url, function(respond, textStatus) {
            respond.images.forEach( function (image) {
                var $output = $(generateTemplate(template, image));

                $uiGalleries
                    .append($output)
                    .masonry('appended', $output);

                reAdjustLayout();
            });

            if ( respond.next !== null ) {
                $uiGalleriesBtn.attr('data-next', respond.next);
            } else {
                $uiGalleriesBtn.hide();
            }
        });
    });

    function reAdjustLayout() {
        $uiGalleries.imagesLoaded().progress( function () {
            $uiGalleries.masonry('layout');
        });
    }

    function generateTemplate( _string, _data ) {
        for ( var prop in _data ) {
            _string = _string.replace(new RegExp('{{' + prop + '}}'), _data[prop]);
        }

        return _string;
    }

});
